import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "@/utils/ajax";
//引入element ui
import "@/utils/elementui";
// 引入全局样式
import "@/assets/global.css";
// 引入自定义指令
import "@/utils/permission";
// 引入图标库组件
import "@/utils/icons";
// 引入Markdown
import "@/utils/markdown"
// 引入代码高亮
import "@/utils/highlight";


Vue.config.productionTip = false;
// 是否在浏览器上开启vue调试工具
Vue.config.devtools = false;

// 挂载七牛云
Vue.prototype.$qiniu = "http://qny.mqqt.xyz/";

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
