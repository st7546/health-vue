import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: sessionStorage.getItem("token") || "",
    name: "",
    avatar: "",
    nickName: "",
    roles: [], // 转回来
    menus: [],
    permissions: [],
  },
  mutations: {
    setToken(state, data) {
      state.token = data;
      sessionStorage.setItem("token", data); // 存入本地会话
    },
    setName(state, data) {
      state.name = data;
    },
    setAvatar(state, data) {
      state.avatar = data;
    },
    setNickName(state, data) {
      state.nickName = data;
    },
    /**
     * 向vuex中存入角色信息
     * @param state
     * @param data
     */
    setRoles(state, data) {
      state.roles = data;
    },
    /**
     * 存入菜单信息
     * @param state
     * @param data
     */
    setMenus(state, data) {
      state.menus = data;
    },
    /**
     * 存入权限数据，限制用户操作
     * @param state
     * @param data
     */
    setPermissions(state, data) {
      state.permissions = data;
    },
  },
  actions: {},
  modules: {},
});
