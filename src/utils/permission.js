/*全局自定义指令*/
import Vue from "vue";
import store from "@/store";

/**
 * directive自定义指令的方法
 * 第一个参数是 指令
 * 第二个是实现方法
 * el: 数组元素 binding： 传入的数据
 */
Vue.directive("hasRole", {
  inserted(el, binding) {
    // 获取指令传过来的数据
    const { value } = binding;
    console.log("获取指令传过来的数据", value);
    // 获取vuex中的角色信息
    const roles = store.state.roles;
    console.log("获取vuex中的角色信息", roles);
    // 定义超级管理有所有权限
    const admin = "SUPER_ADMIN";
    // 判断指令是否传值，传递的值是否是一个数组，数组是否大于0
    console.log("========", value);
    if (value && value instanceof Array && value.length > 0) {
      /**
       * some： 用于检测数组中的元素是否满足指定的条件，不会改变数组 Boole类型
       */
      const hasRole = roles.some((item) => {
        /**
         * include ：用于判断字符串是否包含某个指定的子字符串
         */
        return item.value === admin || value.includes(item.value);
      });
      console.log("hasRole", hasRole);
      // 如果没有该角色
      if (!hasRole) {
        // 就把对应元素删掉
        el.parentNode.removeChild(el);
        console.log("删除成功");
      }
    } else {
      throw new Error(`请设置${value}对应角色标签`);
    }
  },
});

/**
 * 自定义权限指令
 */
Vue.directive("hasPermission", {
  inserted(el, binding) {
    const { value } = binding;
    // 获取权限数据
    const permissions = store.state.permissions;
    // 获取角色信息
    const roles = store.state.roles;
    // 定义超级管理有所有权限
    const admin = "SUPER_ADMIN";
    // 判断指令是否传值，传递的值是否是一个数组，数组是否大于0
    if (value && value instanceof Array && value.length > 0) {
      const hasPermission = permissions.some((item) => {
        return value.includes(item.value);
      });
      const hasRole = roles.some((item) => {
        return item.value === admin;
      });
      if (!hasPermission && !hasRole) {
        el.parentNode.removeChild(el);
        console.log("删除成功");
      }
    } else {
      throw new Error(`请设置${value}对应权限标签`);
    }
  },
});
