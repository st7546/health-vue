/**
 * 邮箱
 * @param {*} s
 */
export function isEmail(s) {
  return /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(s);
}
