import Vue from "vue";
import axios from "axios";
import { Message } from "element-ui";
import router from "@/router";

/**
 * 创建ajax实例
 * 并设置请求超时时间
 * @type {AxiosInstance}
 */
const ajax = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  timeout: 100000,
});

/**
 * 前端请求拦截器
 */
ajax.interceptors.request.use(
  (config) => {
    // 写入 token
    const token = sessionStorage.getItem("token");
    if (token) {
      // 给token 加上 请求头
      config.headers["Authorization"] = token;
    }
    console.log("请求", config);
    return config;
  },
  (error) => {
    console.log("请求异常", error);
  }
);

/**
 * 响应拦截器
 * 既后端响应的信息
 */
ajax.interceptors.response.use(
  (res) => {
    if (!res.data.flag) {
      Message.error(res.data.message);
    }
    return res;
    // console.log(res);
  },
  (err) => {
    // console.log('异常', err.response);
    // 三个等号  绝对等于  0=0  0！='0'  双等号：  0=0,  0 = '0'
    if (err.response.status === 400) {
      Message.error(err.response.data.message);
    } else if (err.response.status === 401) {
      Message.error("您未登录， 请登录后操作!");
      sessionStorage.clear();
      router.replace("/login");
    } else if (err.response.status === 403) {
      Message.error(err.response.data.message);
    } else if (err.response.status === 404) {
      Message.error("后端接口未找到!");
    } else if (err.response.status === 405) {
      Message.error(
        '请求方法错误-->需要"' + err.response.headers.allow + '"请求'
      );
    } else if (err.response.status === 500) {
      Message.error("后端异常-->" + err.response.data.message);
    } else {
      Message.error("未知错误!");
    }
  }
);

//vue全局挂载axios
Vue.prototype.$ajax = ajax;

// 暴露ajax
export default ajax;
