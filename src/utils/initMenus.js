import Home from "@/views/Home";

/**
 * 格式化菜单，将菜单转化为组件
 * filter 创建一个新的数组， 新数组中的元素通过指定数组符合的条件进行返回
 * @param menus 传入菜单信息 （Array）
 */
export const formatMenu = (menus) => {
  // 遍历后台传来的路由字符串, 转换为组件对象, 返回格式化的菜单
  const route = menus.filter((item) => {
    // 处理父级菜单，处理为Home组件
    item.component =
      item.component === "home" ? Home : loadView(item.component);
    item.icon = item.icon;
    item.meta = {
      title: item.title,
    };
    // 处理子菜单
    if (item.children && item.children.length > 0) {
      formatMenu(item.children);
    }
    return true;
  });
  // 将格式化后的菜单进行返回
  return route;
};

/**
 * 路由懒加载  匿名方法
 * @param view 组件路径
 * @returns {function(*): any} 返回路由组件
 */
const loadView = (view) => {
  return (resolve) => require([`@/views/${view}.vue`], resolve);
};
